# movie-api-example
I created this project to learn vue.js. I implemented the project to learn below structures, <br />
transfer data between components in the vuex, <br />
pages are routing  operations with the vue router<br />
and sending API requests with axios. <br>
**Note: You must be use your API key on the project.**
https://www.omdbapi.com/
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
