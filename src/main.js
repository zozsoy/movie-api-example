import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueRouter from 'vue-router'
import {store} from './store'

Vue.config.productionTip = false
Vue.prototype.$http = axios

Vue.use(VueRouter)
import SearchByTitle from "./components/SearchByTitle"
import FavoriteMovies from "./components/FavoriteMovies"
import MovieDetail from "./components/MovieDetail"
import ReviewPage from "./components/ReviewPage"

const router = new VueRouter({
  mode: 'history',
  routes: [
    {path: '/search', component:SearchByTitle},
    {path: '/favorite', component:FavoriteMovies},
    {path: '/detail', component:MovieDetail},
    {path: '/review', component:ReviewPage},
  ]
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
