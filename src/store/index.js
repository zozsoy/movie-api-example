import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        searchRes: [],
        favoriteMovies: [],
        imdbID: null,
        myReviewsAndVotes: [],
    },
    mutations: {
        setSearchRes(state, payload) {
            state.searchRes = payload
        },
        setFavoriteMovies(state, payload) {
            state.favoriteMovies.unshift(payload)
        },
        deleteFavoriteMovie(state, value) {
            state.favoriteMovies.splice(value, 1)
        },
        setImdbID(state, payload) {
            state.imdbID = payload
        },
        setMyReviewsAndVotes(state, payload) {
            state.myReviewsAndVotes.unshift(payload)
        },
        deleteMyReviewsAndVotes(state, value) {
            state.myReviewsAndVotes.splice(value, 1)
        },
    }
})